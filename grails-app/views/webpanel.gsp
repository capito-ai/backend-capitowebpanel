
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Capito Web Panel</title>

    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/webpanel.css" rel="stylesheet">
    <link href="css/prettify.css" type="text/css" rel="stylesheet" />
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Capito Web Panel</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <!--  <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            
          </ul>-->
          <div class="navbar-form navbar-right">
          	<label>Logged in as </label>
            <input id="userId-input" type="text" class="form-control" value="TestUser" disabled>
			<label>API Key </label>
			<div id="connect-formGroup" class="form-group">
              <input id="apiKey-input" type="text" placeholder="API Key" class="form-control" value="<INSERT_API_KEY>" ></input>
            </div>
            
            <button type="button" class="btn btn-danger" id="connect-btn">Connect</button>
          </div>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    
	<div id="errorPanel" class="bg-danger text-center">
	</div>
	
    <div class="container">
      <div class="starter-template">
      
      	<div class="panel-group" id="accordion">
   			    <div class="panel panel-default" id="panel1">
			    	
			    	<div class="panel-heading">
			        	<h4 class="panel-title">
			       			<span data-toggle="collapse" class="collapsed" data-target="#collapseOne">Configuration Options</span>
			  	    	</h4>
			        </div>
			        <div id="collapseOne" class="panel-collapse collapse">
			        
			            <div class="panel-body">
			            		<div class="form-group">
									<span>Language
								    	<select id="language-select" class="form-control" >
										  	<option value="en_GB">English (UK)</option>
										</select> 
									</span>
					            </div>
					            
					            <div class="form-group">
					            	<div class="checkbox">
					            		<label>
											<input type="checkbox" id="staticResponse-chkbox"> Static Response
										</label>
									</div>
								</div>	
			<!-- 			
								<div class="form-group">
									<div class="checkbox">
										<label>
							      			<input type="checkbox" id="useVocab-chkbox" checked> Use Vocab List
							      		</label>
							      	</div>
								</div>	
								
								<div class="form-group">
									<div class="checkbox">
										<label>
							    			<input type="checkbox" id="dictationMode-chkbox"> Dictation Mode
							    		</label>
							    	</div>			
								</div>	
				 -->					  
								<div class="form-group">
									<span>Test Case</span><br>
								    <input type="text"  id="testCase-input" placeholder="Enter Test Case" value="1" disabled>
								</div>	
								
								<div class="form-group">
									<button type="button" class="btn" id="changeConfig-btn">Save Config</button>
								</div>
				            
						</div> <!-- /#panel-body -->
						
			        </div><!-- /#collapseOne -->
			        
			    </div><!-- /#panel1 -->
			    
			    <div class="panel panel-default" id="panel2">
			        <div class="panel-heading">
			        	<h4 class="panel-title">
			        		<span data-toggle="collapse" data-target="#collapseTwo" >Text Submit</span>
			      		</h4>
			        </div>
			        <div id="collapseTwo" class="panel-collapse collapse in">
			         
			            <div class="panel-body">
			         	   	<div class="form-group">
							    <input type="text" class="form-control" id="text-input" placeholder="Enter Text">
					   		</div>	
							<div class="form-group">
								<button type="button" class="btn" id="submitText-btn" disabled>Submit</button>
							</div>	
			           </div>
			           
			        </div>
			    </div>
			    
			    <div class="panel panel-default" id="panel3">
			        <div class="panel-heading">
			        	<h4 class="panel-title">
			        		<span data-toggle="collapse" data-target="#collapseThree" >Context</span>
			      		</h4>
			        </div>
			        <div id="collapseThree" class="panel-collapse collapse in">
			            <div class="panel-body">
			            	<pre id="contextPanel" class="prettyprint"></pre>
			        	</div>
			    	</div>
				</div>
		
			    <div class="panel panel-default" id="panel4">
			    	<div class="panel-heading">
			        	<h4 class="panel-title">
			        		<span data-toggle="collapse" data-target="#collapseFour" href="#collapseFour">Data</span>
			      		</h4>
			        </div>
			        <div id="collapseFour" class="panel-collapse collapse in">
			            <div class="panel-body">
				            <pre id="dataPanel" class="prettyprint"></pre>
			            </div>
			        </div>
			    </div>
			    
			     <div class="panel panel-default" id="panel5">
			    	<div class="panel-heading">
			        	<h4 class="panel-title">
			        		<span data-toggle="collapse" data-target="#collapseFive" href="#collapseFive">Related Search</span>
			      		</h4>
			        </div>
			        <div id="collapseFive" class="panel-collapse collapse in">
			            <div class="panel-body">
				            <pre id="relatedSearchPanel" class="prettyprint"></pre>
			            </div>
			        </div>
			    </div>
			   
			</div> <!-- /.panel-group -->

      </div><!-- /.starter-template -->

    </div><!-- /.container -->

    <script type="text/javascript" src="js/prettify/prettify.js"></script>
    <script src="js/jquery/jquery-2.0.3.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/webpanel/CapitoController.js"></script>
    <script src="js/webpanel/webpanel.js"></script>
    
  </body>
</html>
