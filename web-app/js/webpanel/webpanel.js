var CapitoWebPanel = (function( $, prettyPrint, CapitoController ) {
	
	var errorPanel = $( '#errorPanel' ),
		connectButton = $( '#connect-btn' ),
		submitTextButton = $( '#submitText-btn' ),
		contextPanel = $( '#contextPanel' ),
		dataPanel = $( '#dataPanel' ),
		relatedSearchPanel = $( '#relatedSearchPanel' ),
		changeConfigButton = $( '#changeConfig-btn' ),
		apiKeyInput = $( '#apiKey-input' ),
		submitTextInput = $( '#text-input' ),
		languageSelectInput = $( '#language-select' ),
		staticResponseCheckbox = $( '#staticResponse-chkbox' ),
		testCaseInput = $( '#testCase-input' ),
		userIdInput = $( '#userId-input' ),
		capito = CapitoController.getInstance( {
			apiKey: apiKeyInput.val(),
			language: $( '#language-select' ).val(),
			mode: ( staticResponseCheckbox.is( ':checked' ) ) ? 'SR' : 'T',
			testCase: testCaseInput.val(),
			serverUrl: 'https://sysportal.test.a.cloud.capitosystems.com/'
		});
	
	var toggleConnectButton = function( toggleOn ) {
		if ( toggleOn ) {
			connectButton.removeClass( 'btn-danger' );
			connectButton.addClass( 'btn-success' );
			connectButton.text( 'Disconnect' );
			submitTextButton.prop( 'disabled', false );
		} else {
			connectButton.removeClass( 'btn-success' );
			connectButton.addClass( 'btn-danger' );
			connectButton.text( 'Connect' );
			submitTextButton.prop( 'disabled', true);
		}
	};
	
	var showErrorPanel = function( txt ) {
		errorPanel.text( txt );
		errorPanel.fadeIn();
		setTimeout( function() {
			errorPanel.fadeOut('slow');
		}, 5000);
	};
	//{"asrList":null,"message":{"type":"ERROR","details":null,"text":"Error while saving device","label":null},"status":"NOK","response":{},"code":"500"}
	var disconnect = function() {
		capito.disconnect( function( data ) {
			if ( data.status === 'OK' ) {
				toggleConnectButton( false );
			}
			showErrorPanel( data.message.type + ' : ' + data.code + ' : ' + data.message.text );
		}, function( status, errorThrown ) {
			showErrorPanel( status + ' : ' + errorThrown );
		});
	};
	
	var connect = function() {
		var cfg = capito.config();
		if (cfg.deviceId.slice(0, 4) !== 'CWP_') {
			cfg.deviceId = 'CWP_' + cfg.deviceId; 
			capito.config( cfg );
		}
		capito.connect( function( data ) {
			if ( data.status === 'OK' ) {
				toggleConnectButton( true );
			} else {
				toggleConnectButton( false );
			}
			showErrorPanel( data.message.type + ' : ' + data.code + ' : ' + data.message.text );
		}, function( status, errorThrown ) {
			toggleConnectButton( false );
			showErrorPanel( status + ' : ' + errorThrown );
		});
	};
	
	var submitText = function( text ) {
		contextPanel.text('');
		dataPanel.text('');
		relatedSearchPanel.text('');
		var userId = userIdInput.val();
		capito.text( text, userId, function( data ) {
			if ( data.response.context ) {
				contextPanel.text( JSON.stringify( data.response.context, undefined, 4 ) );
			}
			if ( data.response.data ) {
				dataPanel.text( JSON.stringify( data.response.data, undefined, 4 ) );
			}
			if ( data.response.relatedSearch ) {
				relatedSearchPanel.text( JSON.stringify( data.response.relatedSearch, undefined, 4 ) );
			}
			showErrorPanel( data.message.type  
					+ ((data.code) ? ' : ' + data.code : '')  
					+ ' : ' + data.message.text );
			if (prettyPrint) { 
				prettyPrint();
			}
		}, function(status, errorThrown) {
			showErrorPanel( status + ' : ' + errorThrown );
		});
	};
	
	changeConfigButton.click( function() {
		 var newConfig = {
			language: $( '#language-select' ).val(),
			mode: ( $( '#staticResponse-chkbox' ).is( ':checked' ) ) ? 'SR' : 'T',
			testCase: $( '#testCase-input' ).val()	
		};
		capito.config(newConfig);	
		showErrorPanel( 'Config change has been saved' );
		submitTextButton.prop( 'disabled', false );
	});
	
	connectButton.click( function() {
		if ( connectButton.hasClass( 'btn-danger' ) ) {
			connect();
		} else {
			disconnect();
		}
	});

	submitTextButton.click( function() {
		submitText( submitTextInput.val() );
	});
	
	submitTextInput.keypress( function ( e ) {
		if ( e.which === 13 ) {
			submitText( submitTextInput.val() );
			return false;
		}
	});
	
	apiKeyInput.change( function() {
		var newConfig = {
			apiKey: apiKeyInput.val()
		};
		capito.config( newConfig );
		toggleConnectButton( false );
	});
	
	apiKeyInput.keypress( function ( e ) {
		if ( e.which === 13 ) {
			if ( connectButton.hasClass( 'btn-danger' ) ) {
				connect();
			}
			return false;
		}
	});
	
	staticResponseCheckbox.change( function() {
		 staticResponseCheckbox.is( ':checked' )  ? testCaseInput.prop( 'disabled', false) : testCaseInput.prop( 'disabled', true); 
		 submitTextButton.prop( 'disabled', true );
	});
	
	testCaseInput.change ( function() {
		 submitTextButton.prop( 'disabled', true );
	});
	
	languageSelectInput.change ( function() {
		 submitTextButton.prop( 'disabled', true );
	});
	
}(jQuery, prettyPrint, CapitoController));